squares = map (^2) [1..]
isSquare x = x == (head $ dropWhile (<x) squares)
pythagoreanTriplets = [[a,b,truncate . sqrt . fromIntegral $ a^2 + b^2] | a <- [1..], b <- [1..a-1], isSquare $ a^2 + b^2]

main = print $ product $ head $ filter ((== 1000) . sum) pythagoreanTriplets
