-- This leverages the Fundamental Theorem of Arithmetic, saying that any integer above 1
-- is either a prime number, or can be made by multiplying prime numbers together (composite).
primes :: [Integer]
primes = 2 : filter ((==1) . length . primeFactors) [3..]

-- Factoring a number requires iterating through primes, we do this with a recursive function.
primeFactors :: Integer -> [Integer]
primeFactors n = factor n primes
    where
        factor n (p:ps)
            -- No more divisors, n must be prime
            | p*p > n = [n]
            -- Make sure the number divides evenly, if so, keep the prime that divided, and process the remainder the same way
            | n `mod` p == 0 = p : factor (n `div` p) (p:ps)
            -- If not divisible by our prime number, keep trying with other primes
            | otherwise = factor n ps

main :: IO ()
main = print $ maximum $ primeFactors 600851475143
