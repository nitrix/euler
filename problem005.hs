-- isEvenlyDivisibleByRange range n = all ((==0) . (n `mod`)) range
-- main = print $ head $ filter (isEvenlyDivisibleByRange [1..20]) [1..]

main = print $ foldl lcm 1 [1..20]
