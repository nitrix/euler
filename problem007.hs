-- primes = sieve [2..]
--    where sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p /= 0]

primes = 2 : 3 : filter (isPrime primes) [5, 7 ..]
    where isPrime (p:ps) n = p*p > n || n `rem` p /= 0 && isPrime ps n

main = print $ primes !! 10000
