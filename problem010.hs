primes = 2 : 3 : filter (isPrime primes) [5, 7 ..]
    where isPrime (p:ps) n = p*p > n || n `rem` p /= 0 && isPrime ps n

main = print $ sum $ takeWhile (<2000000) primes
